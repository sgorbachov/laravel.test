<?php

namespace Tests\Feature;

use Tests\TestCase;

/**
 * Class HomeControllerTest
 * @package Tests\Feature
 */
class HomeControllerTest extends TestCase
{
    /**
     * Test view home
     */
    public function testHome()
    {
        $response = $this->get('/test/');
        $response
            ->assertStatus(200)
            ->assertViewIs('search::layout');
    }
}
