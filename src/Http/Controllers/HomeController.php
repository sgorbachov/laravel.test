<?php

namespace Test\Search\Http\Controllers;

use Illuminate\Routing\Controller;

/**
 * Class HomeController
 * @package Test\Search\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Display the Search view.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        return view('search::layout', [
            'cssFile' => 'app.css',
            'assetsAreCurrent' => true
        ]);
    }
}
