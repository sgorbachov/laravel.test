<?php

namespace Test\Search;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

/**
 * Class SearchServiceProvider
 * @package Test\Search
 */
class SearchServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any package services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->registerMigrations();
            $this->registerPublishing();
        }

        $this->registerRoutes();

        $this->loadViewsFrom(
            __DIR__.'/../resources/views', 'search'
        );
    }

    /**
     * Register the package routes.
     *
     * @return void
     */
    private function registerRoutes()
    {
        Route::group($this->routeConfiguration(), function () {
            $this->loadRoutesFrom(__DIR__.'/Http/routes.php');
        });
    }

    /**
     * Get the Telescope route group configuration array.
     *
     * @return array
     */
    private function routeConfiguration()
    {
        return [
            'namespace' => 'Test\Search\Http\Controllers',
            'prefix' => config('search.path', 'test')
        ];
    }

    /**
     * Register the package's migrations.
     *
     * @return void
     */
    private function registerMigrations()
    {
        if (Search::shouldRunMigrations()) {
            $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        }
    }

    /**
     * Register the package's publishable resources.
     *
     * @return void
     */
    private function registerPublishing()
    {
        $this->publishes([
            __DIR__.'/../database/migrations' => database_path('migrations'),
        ], 'search-migrations');

        $this->publishes([
            __DIR__.'/../database/factories' => database_path('factories'),
        ], 'search-factories');

        $this->publishes([
            __DIR__.'/../config/search.php' => config_path('search.php'),
        ], 'search-config');

        $this->publishes([
            __DIR__.'/../public' => public_path('vendor/search'),
        ], 'search-assets');
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/search.php', 'search'
        );

        $this->commands([
            Console\InstallCommand::class,
        ]);
    }
}
