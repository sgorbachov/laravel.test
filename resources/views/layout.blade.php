<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Information -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="robots" content="noindex, nofollow">
    <title>Search</title>
    <!-- Style sheets-->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="{{ asset(mix('app.css', 'vendor/search')) }}" rel="stylesheet" type="text/css">
</head>
<body>
<div id="search" v-cloak>
    <catalog></catalog>
</div>

<script src="{{asset(mix('app.js', 'vendor/search'))}}"></script>
</body>
</html>
