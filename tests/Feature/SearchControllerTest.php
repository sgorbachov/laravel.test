<?php

namespace Tests\Feature;

use Tests\TestCase;

/**
 * Class SearchControllerTest
 * @package Tests\Feature
 */
class SearchControllerTest extends TestCase
{
    /**
     * Test search action
     */
    public function testSearch()
    {
        $response = $this->getJson('/test/api/search/?query=fox&page=2');
        $response
            ->assertStatus(200)
            ->assertJson([
                'page' => 2
            ])
            ->assertJsonStructure([
                'page',
                'count',
                'products' => [
                    ['id', '_id']
                ]
            ]);
    }
}
