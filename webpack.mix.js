const mix = require('laravel-mix');
const webpack = require('webpack');

mix.options({
    terser: {
        terserOptions: {
            compress: {
                drop_console: true,
            },
        },
    },
})
    .setPublicPath('public')
    .js('resources/js/app.js', 'public').sourceMaps()
    .sass('resources/sass/app.scss', 'public')
    .copyDirectory('resources/assets', 'public/assets')
    .copy('public', '../../../public/vendor/search')
    .version()
    .browserSync()
    .webpackConfig({
        resolve: {
            symlinks: false,
            alias: {
                '@': path.resolve(__dirname, 'resources/js/'),
            },
        },
        plugins: [new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)],
    });
