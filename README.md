# Test Search

Test package

## Install

- Install Laravel https://laravel.com/docs/7.x/installation
- Add repository into composer.json
    ```
    "repositories": [
          {
              "type": "vcs",
              "url": "git@gitlab.com:sgorbachov/laravel.test.git"
          }
      ]
    ```
    
- Set in .env DB_CONNECTION and DB_DATABASE (e.g. sqlite3)
- Install package and migrations 
    ```
    composer requre test/search
    php artisan search:install
    php artisan migrate
    ```
- Start a development serve
    ```
    php artisan serve
    ```
- Open http://127.0.0.1:8000/test/
