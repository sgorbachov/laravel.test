<?php

namespace Tests\Feature;

use Test\Search\Catalog;
use Tests\TestCase;

/**
 * Class CatalogControllerTest
 * @package Tests\Feature
 */
class CatalogControllerTest extends TestCase
{
    /**
     * Test catalog create
     */
    public function testCatalogCreate()
    {
        $item = factory(Catalog::class)->make();
        $data = $item->only('name', 'category', 'image', 'xml_id');
        $response = $this->post('/test/api/catalog/', $data);

        $response->assertStatus(201)
            ->assertJson(['data' => $data]);
        $this->assertIsInt($response->json('data.id'));
    }

    /**
     * Test catalog update
     */
    public function testCatalogUpdate()
    {
        $item = factory(Catalog::class)->create();
        $data = $item->only('name', 'category', 'image', 'xml_id');
        $response = $this->post('/test/api/catalog/', $data);

        $response
            ->assertStatus(200)
            ->assertJson(['data' => $data]);
        $this->assertIsInt($response->json('data.id'));
    }
}
