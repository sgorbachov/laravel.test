import Vue from 'vue';
import axios from 'axios';
import VueInsProgressBar from 'vue-ins-progress-bar'

require('bootstrap');

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
}

const options = {
    position: 'fixed',
    show: true,
    height: '5px'
}

Vue.use(VueInsProgressBar, options)

Vue.component('catalog', require('./components/Catalog.vue').default);
Vue.component('catalog-card', require('./components/CatalogCard.vue').default);

new Vue({
    el: '#search'
});
