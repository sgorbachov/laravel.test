<?php

namespace Test\Search\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;

/**
 * Class SearchController
 * @package Test\Search\Http\Controllers
 */
class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Http\Client\RequestException
     */
    public function search(Request $request)
    {
        $query = $request->get('query');
        $page = $request->get('page');

        $response = Http::timeout(5)->get('https://world.openfoodfacts.org/cgi/search.pl', [
            'action' => 'process',
            'search_terms' => $query,
            'sort_by' => 'unique_scans_n',
            'page' => $page,
            'page_size' => 12,
            'json' => 1
        ]);

        return $response->throw()->json();
    }
}
