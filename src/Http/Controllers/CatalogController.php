<?php

namespace Test\Search\Http\Controllers;

use Test\Search\Catalog;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Test\Search\Http\Resources\CatalogCollection;

/**
 * Class CatalogController
 * @package Test\Search\Http\Controllers
 */
class CatalogController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return CatalogCollection
     */
    public function store(Request $request)
    {
        $data = $request->only(['xml_id', 'name', 'image', 'category']);

        $item = Catalog::updateOrCreate(
            ['xml_id' => $request->post('xml_id')],
            $data
        );

        return (new CatalogCollection($item));
    }
}
