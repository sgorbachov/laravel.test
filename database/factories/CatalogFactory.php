<?php

use Test\Search\Catalog;

/* @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Catalog::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'image' => $faker->imageUrl(),
        'category' => $faker->realText(10),
        'xml_id' => $faker->uuid
    ];
});
