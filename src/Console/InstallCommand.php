<?php

namespace Test\Search\Console;

use Illuminate\Console\Command;

/**
 * Class InstallCommand
 * @package Test\Search\Console
 */
class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'search:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install all of the Search resources';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->comment('Publishing Search Service Provider...');
        $this->callSilent('vendor:publish', ['--provider' => 'Test\Search\SearchServiceProvider']);

        $this->info('Search package installed successfully.');
    }
}
