<?php

namespace Test\Search;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Catalog
 * @package Test\Search
 */
class Catalog extends Model
{
    /** @var string[] */
    protected $fillable = ['xml_id', 'name', 'image', 'category'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'catalog';
}
