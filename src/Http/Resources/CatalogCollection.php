<?php

namespace Test\Search\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CatalogCollection
 * @package Test\Search\Http\Resources
 */
class CatalogCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
