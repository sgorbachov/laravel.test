<?php

use Illuminate\Support\Facades\Route;

Route::prefix('api')->group(function () {
    Route::get('/search/{query?}', 'SearchController@search');
    Route::resource('catalog', 'CatalogController')->only([
        'store'
    ]);
});

Route::get('/', 'HomeController@index')->name('Search');
