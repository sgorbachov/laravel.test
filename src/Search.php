<?php

namespace Test\Search;

/**
 * Class Search
 * @package Test\Search
 */
class Search
{
    /**
     * Indicates if Search's migrations will be run.
     *
     * @var bool
     */
    public static $runsMigrations = true;

    /**
     * Determine if Search's migrations should be run.
     *
     * @return bool
     */
    public static function shouldRunMigrations(): bool
    {
        return static::$runsMigrations;
    }
}
